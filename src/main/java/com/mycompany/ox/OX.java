/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.ox;
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        boolean isOturn = true;
        System.out.println("Welcome to OX Game");
        char board[][] = new char[3][3];
        int count = 0;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                board[i][j] = '-';
            }
        }
        while(true){
            for(int i = 0; i < 3; i++){
                for(int j = 0; j < 3; j++){
                    System.out.print(board[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
            int f,g;
            if(isOturn){
                isOturn = false;
                while(true){
                    System.out.println("Turn O");
                    System.out.println("Please input row, col: ");
                    f = kb.nextInt();
                    g = kb.nextInt();
                    try {
                        if(board[f-1][g-1] == '-'){
                            board[f-1][g-1] = 'O';
                            break;
                        }else{
                            System.out.println("Invalid input please try again.");
                            for(int x = 0; x < 3; x++){
                                for(int y = 0; y < 3; y++){
                                    System.out.print(board[x][y] + " ");
                                }
                                System.out.println();
                            }
                            System.out.println();
                        }
                    } catch (Exception e) {
                        System.out.println("Invalid input please try again.");
                            for(int x = 0; x < 3; x++){
                                for(int y = 0; y < 3; y++){
                                    System.out.print(board[x][y] + " ");
                                }
                                System.out.println();
                            }
                            System.out.println();
                    }
                }
            }else{
                isOturn = true;
                while(true){
                    System.out.println("Turn X");
                    System.out.println("Please input row, col: ");
                    f = kb.nextInt();
                    g = kb.nextInt();
                    try {
                        if(board[f-1][g-1] == '-'){
                            board[f-1][g-1] = 'X';
                            break;
                        }else{
                            System.out.println("Invalid input please try again.");
                            for(int x = 0; x < 3; x++){
                                for(int y = 0; y < 3; y++){
                                    System.out.print(board[x][y] + " ");
                                }
                                System.out.println();
                            }
                            System.out.println();
                        }
                    } catch (Exception e) {
                        System.out.println("Invalid input please try again.");
                            for(int x = 0; x < 3; x++){
                                for(int y = 0; y < 3; y++){
                                    System.out.print(board[x][y] + " ");
                                }
                                System.out.println();
                            }
                            System.out.println();
                    }
                }
            }
            count++;
            
            //check 1st rank win
            if(board[2][0] != '-' && board[2][0] == board[2][1] && board[2][1] == board[2][2]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[2][0] + " Win<<<");
                break;
            }
            
            //check 2nd rank win
            if(board[1][0] != '-' && board[1][0] == board[1][1] && board[1][1] == board[1][2]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[1][0] + " Win<<<");
                break;
            }
            
            //check 3rd rank win
            if(board[0][0] != '-' && board[0][0] == board[0][1] && board[0][1] == board[0][2]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[0][0] + " Win<<<");
                break;
            }
            
            //check a file win
            if(board[0][0] != '-' && board[0][0] == board[1][0] && board[1][0] == board[2][0]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[0][0] + " Win<<<");
                break;
            }
            
            //check b file win
            if(board[0][1] != '-' && board[0][1] == board[1][1] && board[1][1] == board[2][1]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[0][1] + " Win<<<");
                break;
            }
            
            //check c file win
            if(board[0][2] != '-' && board[0][2] == board[1][2] && board[1][2] == board[2][2]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[0][2] + " Win<<<");
                break;
            }
            
            //check top-left to bottom-right win
            if(board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[0][0] + " Win<<<");
                break;
            }
            
            //check top-right to bottom-left win
            if(board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>" + board[0][2] + " Win<<<");
                break;
            }
            
            if(count == 9){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println(">>>Draw<<<");
                break;
            }
        }
    }
}
